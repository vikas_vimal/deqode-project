const Sequelize = require('sequelize');
const db = new Sequelize ('userdb', 'bob', 'password', {
    host: 'localhost',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        aquire: 30000,
        idle: 10000
    }
});
module.exports = db;