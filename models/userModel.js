const Sequelize = require('sequelize');

const db = require('../config/dbConfig')

const user = db.define('user',{
    username: {
        type: Sequelize.STRING,
        allowNull:false,
        isRequired: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        isRequired: true,
        validate: { isEmail: true },
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
        isRequired: true,
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
    },
    role: {
        type: Sequelize.STRING,
        defaultValue: "Admin",
    },
    isblocked: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
});

module.exports = user;