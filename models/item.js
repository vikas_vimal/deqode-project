const Sequelize = require('sequelize');

const db = require('../config/dbConfig')

const item = db.define('item',{
    user_id: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING,
        allowNull:false,
        isRequired: true,
    },
    price: {
        type: Sequelize.INTEGER,
        allowNull: false,
        isRequired: true,
    },
    description: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
    },
    quantity: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
});

module.exports = item;