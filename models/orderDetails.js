const Sequelize = require('sequelize');

const db = require('../config/dbConfig')

const item = db.define('item',{
    o_id:{
        type:Sequelize.INTEGER
    },
    name:{
        type:Sequelize.STRING,
        allowNull: false,
    },
    price:{
        type:Sequelize.STRING,
        allowNull:false
    },
    description:{
        type:Sequelize.STRING
    }
})

module.exports = ordersDetails;