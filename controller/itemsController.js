const Sequelize = require("sequelize");
const { decode } = require("jsonwebtoken");
const db = require("../models/item");
const users = require("../models/users");

exports.item_listAll = async (req, res) => {
  const item = await db
    .findAll()
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: "Err executing command " + err })
        .end();
    });
};

exports.sell_items = async (req, res) => {
  let decode = req.userdata;
  const user = await users.findOne({ where: { email: decode } });
  let user_id = user.id;
  const item = await db
    .findAll({ where: { user_id: user_id } })
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: "Err executing command " + err })
        .end();
    });
};

exports.item_create = async (req, res) => {
  let decode = req.userdata;
  const user = await users.findOne({ where: { email: decode } });
  let user_id = user.id;
  const { name, price, description, quantity } = req.body;
  item = new db({
    name,
    price,
    description,
    quantity,
    user_id: user_id,
  });
  await item
    .save()
    .then(() => {
      return res.redirect("/sell");
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: "Err executing command " + err })
        .end();
    });
};

exports.item_list = async (req, res) => {
  let decode = req.userdata;
  const user = await users.findOne({ where: { email: decode } });
  let user_id = user.id;
  const item = await db
    .findAll({
      where: {
        user_id: {
          [Sequelize.Op.not]: user_id,
        },
      },
    })
    .then((item) => {
      res.send(item);
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: "Err executing command " + err })
        .end();
    });
};
