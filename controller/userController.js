const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const User = require("../models/users");

exports.login_get = (req, res) => {
  return res.json({ message: "login first" });
};
exports.register_get = (req, res) => {
  res.json({ message: "register_first" });
};

exports.login_Post = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email: email } });
    if (!user) {
      res.json({ message: "invalid user" });
      return;
    }
    if (user.isblocked == false) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        const token = jwt.sign(email, process.env.SECRETE_KEY);
        return res.status(200).json({
          token: token,
        });
      }
    }
    return res.status(403).json({ message: "Sorry you are blocked " });
  } catch (error) {
    res
      .status(400)
      .json({
        message: "bad request" + error,
      })
      .end();
  }
};

exports.register_post = async (req, res) => {
  try {
    const { username, email, password } = req.body;
    let user = await User.findOne({ where: { email: email } });
    if (user) {
      return res.redirect("/login");
    }

    const hsdpass = await bcrypt.hash(password, 12);

    user = new User({
      username,
      email,
      password: hsdpass,
    });

    await user.save();
    return res.status(201).json({ message: "user registered" });
  } catch (error) {
    res
      .status(400)
      .json({
        message: 'bad request' + error,
      })
      .end();
  }
};
