const Sequelize = require("sequelize");

const users = require("../models/users");
const items = require("../models/item");
const carts = require("../models/cart");

exports.list_user = async (req, res) => {
  let decode = req.userdata;
  const superUser = await users.findOne({ where: { email: decode } });
  if (superUser.role == "SuperAdmin") {
    const user = await users
      .findAll({
        where: {
          email: {
            [Sequelize.Op.not]: superUser.email,
          },
        },
      })
      .then((users) => {
        return res.status(200).json(users);
      })
      .catch((err) => {
        res
          .status(401)
          .json({ message: "Err executing command " + err })
          .end();
      });
  } else {
    res
      .status(401)
      .json({ message: "sorry You not have right to list users data" })
      .end();
  }
};

exports.block_user = async (req, res) => {
  const emailBlock = req.body.email;
  let decode = req.userdata;
  const superUser = await users.findOne({ where: { email: decode } });
  if (superUser.role == "SuperAdmin") {
    // console.log(emailBlock)
    const user = await users
      .update(
        { isblocked: true },
        {
          where: {
            email: emailBlock,
          },
        }
      )
      .then(() => {
        return res
          .status(200)
          .json({ message: "user blocked related to this: " + emailBlock });
      })
      .catch((err) => {
        res
          .status(500)
          .json({ message: "err executing command " + err })
          .end();
      });
  } else {
    res
      .status(401)
      .json({ message: "sorry You not have right to list users data" })
      .end();
  }
};

exports.unblock_user = async (req, res) => {
  const emailBlock = req.body.email;
  let decode = req.userdata;
  const superUser = await users.findOne({ where: { email: decode } });
  if (superUser.role == "SuperAdmin") {
    const user = await users
      .update(
        { isblocked: false },
        {
          where: {
            email: emailBlock,
          },
        }
      )
      .then(() => {
        return res
          .status(200)
          .json({ message: "user unblocked related to this: " + emailBlock });
      })
      .catch((err) => {
        res
          .status(500)
          .json({ message: "Err executing command " + err })
          .end();
      });
  } else {
    res
      .status(401)
      .json({ message: "sorry You not have right to list users data" })
      .end();
  }
};

exports.delete_user = async (req, res) => {
  const emailDelete = req.body.email;
  let decode = req.userdata;
  const superUser = await users.findOne({ where: { email: decode } });
  if (superUser.role == "SuperAdmin") {
    let user = await users.findOne({ where: { email: email } });
    if (user) {
      await user.destroy({
        where: { email: email },
      });
      return res.json({
        message: "user deleted",
      });
    }
  }
};

exports.delete_product = async (req, res) => {
  const { product_id } = req.body;
  let decode = req.userdata;
  const superUser = await users.findOne({ where: { email: decode } });
  if (superUser.role == "SuperAdmin") {
    let item = await items
      .findOne({ where: { id: product_id } })
      .then(async (items) => {
        await items.destroy({
          where: { id: product_id },
        });
        return res.status(200).json({
          message: "item deleted",
        });
      })
      .catch((err) => {
        res.status(400).json({ message: "please provide correct product id" }).end();
      });
  }
  return res.status(401).json({ message: "sorry you have no rights" });
};
