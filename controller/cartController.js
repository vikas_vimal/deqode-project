const Sequelize = require("sequelize");

const db = require("../config/dbConfig");
const users = require("../models/users");
const cart = require("../models/cart");
const items = require("../models/item");

exports.add_to_cart = async (req, res) => {
  const { product_id } = req.body;
  let decode = req.userdata;
  const user = await users.findOne({ where: { email: decode } });
  let user_id = user.id;
  const product = await items.findOne({ where: { id: product_id } });
  const cartitem = await cart.findOne({
    where: { product_id: product_id, usr_id: user_id },
  });

  if (cartitem) {
    if (cartitem.quantity < product.quantity) {
      cartitem.quantity += 1;
      await cart.update(
        { quantity: cartitem.quantity },
        { where: { product_id: product_id, usr_id: user_id } }
      );
      return res.end();
    } else {
      return res.json({ message: "product quantity is no more in list" });
    }
  } else {
    const item = new cart({
      product_id,
      usr_id: user_id,
    });
    await item.save();
    res.status(201).json({ message: "item saved to cart" }).end();
  }
};

exports.list_my_cartItem = async (req, res) => {
  let decode = req.userdata;
  const user = await users.findOne({ where: { email: decode } });
  let user_id = user.id;
  const items = await db
    .query(
      `SELECT * FROM items as P INNER JOIN carts AS C ON P.id = C.product_id WHERE usr_id = ${user_id}`
    )
    .then((item) => {
      res.send(item[0]);
    })
    .catch((err) => {
      res
        .status(500)
        .json({ message: "Err executing command " + err })
        .end();
    });
};

exports.delete_my_cartItem = async (req, res) => {
  const { product_id } = req.body;
  let decode = req.userdata;
  const user = await users.findOne({ where: { email: decode } });
  let user_id = user.id;
  const cartitem = await cart
    .findOne({ where: { product_id: product_id, usr_id: user_id } })
    .then(async (item) => {
      await item.destroy({
        where: { product_id: product_id },
      });
      return res.redirect("/listcartitem");
    });
};
