const orders = require("../models/orders");
const ordersDetails = require("../models/orderDetails");
const items = require("../models/item");
const carts = require("../models/cart");
const db = require("../config/dbConfig");
const users = require("../models/users");
const user = require("../models/users");

exports.create_order = async (req, res) => {
  try {
    let decode = req.userdata;
    const user = await users.findOne({ where: { email: decode } });
    const item = await items.findAll();
    const cart = await db.query(
      `SELECT * FROM items as P INNER JOIN carts AS C ON P.id = C.product_id WHERE usr_id = ${user.id}`
    );
    const cartValues = cart[0];
    const total = cartValues.reduce((total, item) => {
      return item.price + total;
    }, 0);
    const order = new orders({
      userid: user.id,
      total_amount: total,
    });
    const or = await order.save();

    cartValues.forEach(async (element) => {
      const orderDetail = new ordersDetails({
        o_id: or.dataValues.id,
        name: element.name,
        price: element.price,
        description: element.description,
      });
      await orderDetail.save();
      const product = await items.findOne({
        where: { id: element.product_id },
      });
      if (element.quantity == product.quantity) {
        await items.destroy({ where: { id: element.product_id } });
      } else {
        const finalQuantity = product.quantity - element.quantity;
        await items.update(
          { quantity: finalQuantity },
          { where: { id: element.product_id } }
        );
      }
      await carts.destroy({
        where: { product_id: element.product_id },
      });
    });
    return res.json({ message: "you have paid total amount: " + total });
  } catch (error) {
    res.status(500).json({ message: "executing in " + error });
  }
};

exports.list_my_order = async (req, res) => {
  try {
    let decode = req.userdata;
    const user = await users.findOne({ where: { email: decode } });
    const myorder = await orders.findAll({ where: { userid: user.id } });
    res.json(myorder);
  } catch (error) {
    return res.status(400).json({
      message: "executing in " + error,
    });
  }
};

exports.list_order_detail = async (req, res) => {
  try {
    const { o_id } = req.body;
    const myorder = await ordersDetails.findAll({ where: { o_id: o_id } });
    res.json(myorder);
  } catch (error) {
    return res.status(400).json({
      message: "executing in " + error,
    });
  }
};
