const express = require('express');
const bodyParser = require('body-parser');


const db = require('./config/dbConfig')
const userRouter = require('./routes/userRoutes')
const itemRouter = require('./routes/itemRoutes');
const cartRouter = require('./routes/cartRoutes')
const superAdminRouter = require('./routes/superAdminRoutes')

const app = express()
app.set('view engine', 'ejs');
app.use(bodyParser.json())
// app.use(express.urlencoded({ extended: false }));

db.authenticate().then(() => {
    console.log('Database connected...');
}).catch(err => {
    console.log('Error: ' + err);
});

app.use(itemRouter);
app.use(userRouter);
app.use(cartRouter);
app.use(superAdminRouter)

app.listen('5000',()=>{
    console.log('app running on port 5000')
});
