const router = require('express').Router()
const user = require('../controller/userController')

router.get('/login',user.login_get)
router.post('/login',user.login_Post)
router.get('/register',user.register_get)
router.post('/register',user.register_post)


module.exports = router;
