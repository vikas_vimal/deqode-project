const router = require('express').Router();
const items = require('../controller/itemsController');
const checkAuth = require('../middleware/checkAuth');

router.get('/listall',items.item_listAll)
router.get('/list',checkAuth.checkAuth,items.item_list)
router.get('/sell',checkAuth.checkAuth, items.sell_items)
router.post('/create',checkAuth.checkAuth,items.item_create)

module.exports = router;