const router = require('express').Router();
const carts = require('../controller/cartController');
const checkAuth = require('../middleware/checkAuth');

router.get('/listcartitem',checkAuth.checkAuth, carts.list_my_cartItem);
router.post('/addtocart', checkAuth.checkAuth, carts.add_to_cart)


module.exports = router;