const router = require('express').Router()
const SuperAdmin = require('../controller/superAdminController');
const checkAuth = require('../middleware/checkAuth');

router.get('/listusers',checkAuth.checkAuth,SuperAdmin.list_user)
router.post('/blockuser',checkAuth.checkAuth,SuperAdmin.block_user)
router.post('/unblockuser',checkAuth.checkAuth,SuperAdmin.unblock_user)
router.post('/deleteuser',checkAuth.checkAuth,SuperAdmin.delete_user)
router.post('/deleteproduct',checkAuth.checkAuth,SuperAdmin.delete_product)

 
module.exports = router;